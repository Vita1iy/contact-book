<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    public function index()
    {
        $contacts = \App\Contact::orderBy('name')->paginate(3);

        return view('contacts', [
            'contacts' => $contacts
        ]);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'phone' => 'required|max:20'
        ]);
        if ($validator->fails()) {
            return redirect('/')->withInput()->withErrors($validator);
        }

        $contact = new \App\Contact;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->save();
        return redirect('/');
    }

    public function del(\App\Contact $contact)
    {
        $contact->delete();
        return redirect('/');
    }
}
