<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exports\InvoicesExport;
use PDF;

class UploadController extends Controller
{

    public function excel()
    {
        return \Excel::download(new InvoicesExport, 'contacts.xlsx');
    }

    public function pdf()
    {
        $contacts = Contact::select('name', 'phone')->get();
        $pdf = PDF::loadView('pdf', ['contacts' => $contacts]);
        return $pdf->download('contacts.pdf');
    }

    public function csv()
    {
        return \Excel::download(new InvoicesExport, 'contacts.csv');
    }
}
