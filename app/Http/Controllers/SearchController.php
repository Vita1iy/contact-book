<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $error = ['error' => 'По Вашему запросу ничего не найдено.'];

        if($request->has('q')) {
            $contacts = Contact::search($request->get('q'))->get();

            return $contacts->count() ? $contacts : $error;
        }

        return $error;
    }
}
