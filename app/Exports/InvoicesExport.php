<?php
/**
 * Created by PhpStorm.
 * User: shevc
 * Date: 06.06.2018
 * Time: 1:35
 */

namespace App\Exports;

use App\Contact;


class InvoicesExport implements \Maatwebsite\Excel\Concerns\FromCollection
{
    public function collection()
    {
        return Contact::select('name', 'phone')->get();
    }
}