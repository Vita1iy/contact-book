new Vue({
    el: 'body',
    data: {
        contacts: [],
        loading: false,
        error: false,
        query: ''
    },
    methods: {
        search: function() {
            this.error = '';
            this.contacts = [];
            this.loading = true;

            this.$http.get('/search?q=' + this.query).then((response) => {
                response.body.error ? this.error = response.body.error : this.contacts = response.body;

                this.loading = false;
                this.query = '';
            });
        }
    }
});