<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', 'ContactsController@index');
Route::post('/contact', 'ContactsController@add');
Route::delete('/contact/{contact}', 'ContactsController@del');

Route::get('/search', 'SearchController@search');

Route::get('/upload/excel', 'UploadController@excel');
Route::get('/upload/pdf', 'UploadController@pdf');
Route::get('/upload/csv', 'UploadController@csv');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
