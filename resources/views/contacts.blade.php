@extends('layouts.app')

@section('content')
    @guest
        <h1>Необходимо авторизоваться!</h1>
    @else
        <div class="container">
            <div class="card-body">
                @include('errors')

                <form action="{{ url('contact') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group">
                            <label for="Contact" class="col-sm-3 control-label">Контакт:</label>

                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" name="name" id="user-name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="col-sm-4">
                                    <input type="tel" name="phone" id="user-phone" class="form-control" placeholder="Телефон">
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success">Добавить контакт</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if(count($contacts) > 0)
            <div class="container">
                <div class="card" style="text-align: center;">
                    <div class="card-head">
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-4">
                                <a href="{{ url('upload/excel') }}" class="btn btn-primary">
                                    Выгрузить в Excel
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="{{ url('upload/pdf') }}" class="btn btn-primary">
                                    Выгрузить в pdf
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="{{ url('upload/csv') }}" class="btn btn-primary">
                                    Выгрузить в csv
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Имя</th>
                                <th>Телефон</th>
                            </thead>

                            <tbody>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td class="table-text">
                                            <div>
                                                {{ $contact-> name }}
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                {{ $contact-> phone }}
                                            </div>
                                        </td>
                                        <td>
                                            <form action="{{ url('contact/'.$contact->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button class="btn btn-danger">
                                                    Удалить
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $contacts->links() }}
                    </div>
                </div>
            </div>
        @endif

    @endguest
@endsection