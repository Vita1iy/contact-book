Контактная книжка, реализованная при помощи фреймворка Laravel. Включает в себя сортировку по имени, поиск контакта, пагинацию страниц контактов.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
