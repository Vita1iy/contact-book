<?php

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(),
        'phone' => $faker->numberBetween(111111111, 999999999)
    ];
});
